package lab;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class Thread2 extends Thread{
	CopyOnWriteArrayList<String> s;

	public Thread2(CopyOnWriteArrayList<String> r) {
		this.s =r;
	}

	public void run() {
		synchronized (s) {
			for(String i:s) {
				System.out.println();
				System.out.print(i+" ");
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.getMessage();
				}
			}
		}
	}
}
