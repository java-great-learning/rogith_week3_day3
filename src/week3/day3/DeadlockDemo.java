package week3.day3;

public class DeadlockDemo {
	public static void main(String[] args) {
		Track track1 = new Track("TRACK 1");
		Track track2 = new Track("TRACK 2");
		
		new Thread() {
			public void run() {
				synchronized (track1) {
					track1.move();
					synchronized (track2) {
						track2.move();
					}
					
				}
			}
		}.start();
		
		new Thread() {
			public void run() {
				synchronized (track2) {
					track1.move();
					synchronized (track1) {
						track2.move();
					}
					
				}
			}
		}.start();

	}

}
class Track{
	private String name;

	public Track(String name) {
		super();
		this.name = name;
	}

	public void move()
	{
		System.out.println(Thread.currentThread().getName()+" moving on "+name);
	}
}


