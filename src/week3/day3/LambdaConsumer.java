package week3.day3;

import java.io.File;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LambdaConsumer {
	
	public static void main(String args[]) {
	Consumer<String> consumer = new Consumer<String>() {

		@Override
		public void accept(String t) {
			// TODO Auto-generated method stub
			String extension = t.substring(t.lastIndexOf('.')+1);
			System.out.printf("'%s' %n", extension);
		}
	};
	
	File file = new File("Dog.txt");
	
	consumer.accept("Dog.txt");
	
	String d = "Dog.txt";
	Consumer<String> c1 = (String a) -> {String ext = a.substring(a.lastIndexOf('.') +1);
	System.out.printf("'%s '%n", ext);
	};
	
	c1.accept(d);
	
	BiConsumer<String, Integer> b1 = (s,i) -> {
		if(s.length()== i)
		{
			System.out.println("equal");
		}
		else
		{
			System.out.println("not equal");
		}
	};
	
	b1.accept("Rogith", 6);
	
	
	Supplier<String> sp = () -> {return "Rogith";};
	String str = sp.get();
	System.out.println(str);
	
	
	Predicate<Integer> p1 = (Integer a) -> a%2==0;
	System.out.println(p1.test(4));
}

}